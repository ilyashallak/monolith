package de.mstock.monolith.service;

import java.math.BigDecimal;
import java.util.*;

import ch.qos.logback.core.net.SyslogOutputStream;
import de.mstock.monolith.config.I18nConfig;
import de.mstock.monolith.domain.*;
import de.mstock.monolith.domain.Product;
import de.mstock.warenwirtschaft.models.*;
import de.mstock.warenwirtschaft.net.MicroServiceWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.mstock.monolith.web.CategoryDTO;
import de.mstock.monolith.web.ProductDTO;

@Service
public class ShopService {

  @Autowired
  private CategoryRepository categoryRepository;

  @Autowired
  private ProductRepository productRepository;

  @Autowired
  private DataTransferObjectFactory dtoFactory;

  /**
   * Gets all categories of the current language.
   * 
   * @return A simplified Data Transfer Object.
   */
  public List<CategoryDTO> getCategories(Locale locale) {
    String language = locale.getLanguage();
    List<CategoryDTO> categories = new ArrayList<>();
    for (Category category : categoryRepository.findAllOrdered(language)) {
      categories.add(dtoFactory.createCategoryDTO(category, locale));
    }

    return Collections.unmodifiableList(categories);
  }

  /**
   * Gets all products for a category in the current language.
   * 
   * @return A simplified Data Transfer Object.
   */
  public List<ProductDTO> getProductsForCategory(Locale locale, String prettyUrlFragment) {
    String language = locale.getLanguage();
    Category category = categoryRepository.findByPrettyUrlFragment(language, prettyUrlFragment);
    if (category == null) {
      // produkte aus MS laden
      List<de.mstock.warenwirtschaft.models.Product> msProducts = MicroServiceWrapper.GetProductsByCategory(prettyUrlFragment);
      List<Product> monolithProducts = new ArrayList<>();

      for (de.mstock.warenwirtschaft.models.Product msProduct : msProducts) {
        Product monolithProduct = new Product();
        Map<String, ProductI18n> nameMapping = new HashMap<>();
        ProductI18n productI18n = new ProductI18n();
        productI18n.setName(msProduct.name);
        productI18n.setPrettyUrlFragment(msProduct.name.replaceAll("[^a-zA-Z0-9]",""));
        productI18n.setDescription(msProduct.name);
        productI18n.setPrice(BigDecimal.valueOf(msProduct.price));
        nameMapping.put("de", productI18n);
        monolithProduct.setI18n(nameMapping);

        monolithProducts.add(monolithProduct);
      }

      return dtoFactory.createProductWithoutReviewsDTOs(monolithProducts, locale);
    }
    List<ProductDTO> products =
        dtoFactory.createProductWithoutReviewsDTOs(category.getProducts(), locale);
    return Collections.unmodifiableList(products);
  }

  /**
   * Gets a product in the current language.
   * 
   * @return A simplified Data Transfer Object.
   */
  public ProductDTO getProduct(Locale locale, String prettyUrlFragment) {
    Product product = productRepository.findByI18nName(locale.getLanguage(), prettyUrlFragment);
    if (product == null) {
      de.mstock.warenwirtschaft.models.Product msProduct = MicroServiceWrapper.GetProductByName(prettyUrlFragment);

      Product monoProduct = new Product();
      ProductI18n productI18n = new ProductI18n();
      productI18n.setPrice(BigDecimal.valueOf(msProduct.price));
      productI18n.setName(msProduct.name);
      productI18n.setDescription(msProduct.name);
      productI18n.setPrettyUrlFragment(msProduct.name.replaceAll("[^a-zA-Z0-9]", ""));
      productI18n.setReviews(new ArrayList<>());

      Map<String, ProductI18n> mapping = new HashMap<>();
      mapping.put("de", productI18n);
      monoProduct.setI18n(mapping);

      return dtoFactory.createProductDTO(monoProduct, locale);
    }

    return dtoFactory.createProductDTO(product, locale);
  }

}
