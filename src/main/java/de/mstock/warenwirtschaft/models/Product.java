package de.mstock.warenwirtschaft.models;

public class Product {
    public String Id;
    public String name;
    public String category;
    public int deliverTime;
    public int count;
    public double price;
}
