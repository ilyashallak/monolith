package de.mstock.warenwirtschaft.net;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import de.mstock.warenwirtschaft.models.Product;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.json.GsonJsonParser;

import java.util.ArrayList;
import java.util.List;

public class MicroServiceWrapper {
    private static Gson gson = new Gson();

    public static List<Product> GetProductsByCategory(String category) {
        try {
            HttpResponse<JsonNode> response = Unirest.get("http://localhost:5000/api/items/" + category).asJson();
            JSONArray arr = response.getBody().getArray();
            List<Product> products = new ArrayList<>();

            for(int i = 0; i < arr.length(); i++) {
                Product p = gson.fromJson(arr.getJSONObject(i).toString(),Product.class);
                products.add(p);
            }
            return products;
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static Product GetProductByName(String name) {
        try {
            HttpResponse<JsonNode> response = Unirest.get("http://localhost:5000/api/items/name/" + name).asJson();
            return gson.fromJson(response.getBody().toString(), Product.class);
        } catch (UnirestException e) {
            e.printStackTrace();
            return null;
        }
    }
}
