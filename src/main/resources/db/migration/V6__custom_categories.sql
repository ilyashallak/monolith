insert into categories (id, ordinal) VALUES
  (3, 30),
  (4, 40),
  (5, 50),
  (6, 60),
  (7, 70);

insert into categories_i18n (category_id, locale_language, name, pretty_url_fragment) values
  (3, 'en', 'Food', 'food'),
  (3, 'de', 'Nahrungsmittel', 'nahrungsmittel'),
  (4, 'en', 'Clothing', 'clothing'),
  (4, 'de', 'Kleidung', 'kleidung'),
  (5, 'en', 'Furniture', 'furniture'),
  (5, 'de', 'Möbel', 'moebel'),
  (6, 'en', 'Electronics', 'electronics'),
  (6, 'de', 'Elektronik', 'elektronik'),
  (7, 'en', 'Other', 'other'),
  (7, 'de', 'Anderes', 'anderes');
